package org.example.service;

import org.example.dto.CountryInfoDto;
import org.example.dto.CountryInfoResponseDto;
import org.example.service.feign.CountryInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryInfoServiceImpl implements CountryInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CountryInfoServiceImpl.class.getCanonicalName());
    @Autowired
    private CountryInfoService countryInfoService;

    /**
     * Do an countryInfo GET request.
     * @param country country
     * @return CountryInfoDto[]
     */
    @Override
    public CountryInfoDto[] countryInfo(String country) {
        LOGGER.debug("Init method");
        CountryInfoDto[] response = null;
        try {
            response = countryInfoService.countryInfo(country);
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
        }
        return response;
    }
}
