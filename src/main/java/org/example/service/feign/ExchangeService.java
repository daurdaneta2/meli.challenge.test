package org.example.service.feign;

import org.example.dto.ExchangeResponseDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface ExchangeService {
    @GetMapping(
            path = "/latest",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    ExchangeResponseDto exchange(@RequestParam(name = "base") String base);
}
