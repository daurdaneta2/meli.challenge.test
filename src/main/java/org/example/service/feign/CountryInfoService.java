package org.example.service.feign;

import org.example.dto.CountryInfoDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface CountryInfoService {
    @GetMapping(
            path = "/rest/v2/name/{country}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    CountryInfoDto[] countryInfo(@PathVariable(name = "country") String country);

}
