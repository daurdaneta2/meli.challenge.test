package org.example.service;

import org.apache.commons.lang3.StringUtils;
import org.example.dao.RedisThreadDao;
import org.example.dto.CountryDataDto;
import org.example.dto.CountryInfoDto;
import org.example.dto.CountryInfoResponseDto;
import org.example.dto.CountryLanguagesDto;
import org.example.dto.ExchangeResponseDto;
import org.example.dto.IpDirectionResponseDto;
import org.example.dto.IpInformationResponseDto;
import org.example.dto.ReportResponseDto;
import org.example.exceptions.IpNotFoundException;
import org.example.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class AppService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppService.class.getCanonicalName());
    private static final Double[] ORIGIN_COORD = {-34d, -64d};
    private static final SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Autowired
    private Utils utils;

    @Autowired
    private IpCountryServiceImpl ipCountryServiceImpl;
    @Autowired
    private CountryInfoServiceImpl countryInfoServiceImpl;
    @Autowired
    private ExchangeServiceImpl exchangeServiceImpl;

    @Value("${feign.ip.country.url}")
    private String ipCountryUrl;

    @Autowired
    private RedisThreadDao redis;

    /**
     * Process an IP and get regional information.
     * @param ip ip
     * @return IP Information
     * @throws IpNotFoundException when not found information
     */
    public ResponseEntity<IpInformationResponseDto> processIp(String ip) throws IpNotFoundException {
        LOGGER.debug("Init method");
        IpInformationResponseDto response;
        String uri = ipCountryUrl + "/ip?" + ip;
        URI baseUri = getBaseUri(uri);
        LOGGER.debug("URI : {}", baseUri.toString());
        IpDirectionResponseDto direction = ipCountryServiceImpl.ipCountry(baseUri);
        if (direction == null || StringUtils.isBlank(direction.getCountryName())) {
            throw new IpNotFoundException();
        }

        response = iterateIpCountry(direction, ip);
        String countryName = direction.getCountryName();
        LOGGER.debug("Country name {}", countryName);
        CountryInfoDto[] country = countryInfoServiceImpl.countryInfo(countryName);
        CountryInfoDto finalCountry = null;
        LOGGER.debug("country size {}", country.length);
        if (country.length == 1) {
            finalCountry = country[0];
        } else {
            for (CountryInfoDto item : country) {
                if (direction.getCountryCode3().equals(item.getAlpha3Code())) {
                    LOGGER.debug("code {} - code country {}", direction.getCountryCode3(), item.getAlpha3Code());
                    finalCountry = item;
                }
            }
            if (finalCountry == null) {
                throw new IpNotFoundException();
            }
        }

        if (finalCountry == null || finalCountry.getLatlng() == null) {
            throw new IpNotFoundException();
        }

        String distance = iterateCountryInfo(response, finalCountry);

        if (finalCountry.getCurrencies()[0] != null && finalCountry.getCurrencies()[0].getCode() != null) {
            LOGGER.debug("OK Currency {}", finalCountry.getCurrencies()[0].getCode());
            ExchangeResponseDto exchange = exchangeServiceImpl.exchange(finalCountry.getCurrencies()[0].getCode());
            iterateExchange(response, exchange, country);
        }
        redis.incCountriesData(direction.getCountryName(), distance);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Generate reports of consume.
     * @return ResponseEntity
     */
    public ResponseEntity<ReportResponseDto> generateReport() {
        LOGGER.debug("Init method");
        ResponseEntity<ReportResponseDto> response;
        Set<String> countryData = redis.getDistanceRegistry();
        if (countryData == null || countryData.isEmpty()) {
            response = new ResponseEntity<>(new ReportResponseDto("NOT_FOUND"), HttpStatus.NOT_FOUND);
        } else {
            List<CountryDataDto> countries = redis.getCountriesData(countryData);
            if (countries == null) {
                response = new ResponseEntity<>(new ReportResponseDto("INTERNAL_SERVER_ERROR"),
                        HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (countries.isEmpty()) {
                response = new ResponseEntity<>(new ReportResponseDto("NOT_FOUND"), HttpStatus.NOT_FOUND);
            } else {
                response = new ResponseEntity<>(iterateReportData(countries), HttpStatus.OK);
            }
        }
        return response;
    }

    /**
     * Iterate Redis Data.
     * @param countries countries
     * @return ReportResposneDto
     */
    private ReportResponseDto iterateReportData(List<CountryDataDto> countries) {
        LOGGER.debug("Init method");
        ReportResponseDto response = new ReportResponseDto();
        Integer longDistance = null;
        String longDistanceCountry = null;
        Integer shortDistance = null;
        String shortDistanceCountry = null;
        int averageDistance = 0;
        int iterationCount = 0;
        for (CountryDataDto country : countries) {
            if (longDistance == null || longDistance < country.getDistance()) {
                longDistance = country.getDistance();
                longDistanceCountry = country.getCountry();
            }
            if (shortDistance == null || shortDistance > country.getDistance()) {
                shortDistance = country.getDistance();
                shortDistanceCountry = country.getCountry();
            }
            averageDistance += country.getDistance() * country.getInvoice();
            iterationCount += country.getInvoice();
        }
        LOGGER.debug("average distance {} - iterations {}", averageDistance, iterationCount);
        response.setLongDistance(longDistance + " km");
        response.setShortDistance(shortDistance + " km");
        response.setLongDistanceCountry(longDistanceCountry);
        response.setShortDistanceCountry(shortDistanceCountry);
        response.setAverageDistance((averageDistance / iterationCount) + " km");
        response.setMessage("OK");

        return response;
    }

    /**
     * Iterate Ip basic info.
     * @param direction direction
     * @param ip ip
     * @return IpInformation
     */
    private IpInformationResponseDto iterateIpCountry(IpDirectionResponseDto direction, String ip) {
        LOGGER.debug("Init method");
        IpInformationResponseDto response = new IpInformationResponseDto();
        response.setIp(ip);
        response.setDate(df.format(new Date()));
        response.setName(direction.getCountryName());
        response.setIsoName(direction.getCountryCode3());
        return response;
    }

    /**
     * Iterate Country information.
     * @param response response
     * @param country country
     * @return String
     */
    private String iterateCountryInfo(IpInformationResponseDto response, CountryInfoDto country)
            throws IpNotFoundException {
        LOGGER.debug("Init method");
        if (country.getTranslations().get("es") != null) {
            response.setName(String.format("%s (%s)", country.getTranslations().get("es"), response.getName()));
        }
        if (country.getLanguages() != null) {
            List<String> langs = new ArrayList<>();
            for (CountryLanguagesDto lang : country.getLanguages()) {
                langs.add(String.format("%s (%s | %s)", lang.getNativeName(), lang.getName(), lang.getIso639_1()));
            }
            if (!langs.isEmpty()) {
                response.setLanguages(new String[langs.size()]);
                langs.toArray(response.getLanguages());
            }
        }

        if (country.getTimezones() != null) {
            List<String> hours = new ArrayList<>();
            for (String time : country.getTimezones()) {
                String finalT;
                if(time.equals("UTC")) {
                    finalT = "+00:00";
                } else {
                    finalT = time.replace("UTC", "");
                }
                hours.add(String.format("%s (%s)", utils.getTimeToTimezone(finalT), time));
            }
            if (!hours.isEmpty()) {
                response.setHourZone(new String[hours.size()]);
                hours.toArray(response.getHourZone());
            }
        }
        String distance = null;

        if(country.getLatlng() == null || country.getLatlng().length < 2) {
            throw new IpNotFoundException();
        } else {
            distance = "" + utils.takeDistanceLatLng(ORIGIN_COORD, country.getLatlng()).intValue();
            response.setDistance(distance + " km");
        }



        return distance;
    }

    /**
     * Iterate exchange information.
     * @param response response
     * @param exchange exchange
     * @param country country
     */
    private void iterateExchange(IpInformationResponseDto response, ExchangeResponseDto exchange,
                                 CountryInfoDto[] country) {
        LOGGER.debug("Init method");
        if (exchange != null && country[0].getCurrencies()[0].getCode().equals(exchange.getBase())) {
            response.setCurrency(String.format("%s | %s%s",
                    country[0].getCurrencies()[0].getName(), country[0].getCurrencies()[0].getCode(),
                    exchange.getRates().containsKey("USD")
                            ? String.format("(1 %s = %s USD)", country[0].getCurrencies()[0].getCode(),
                            1 / exchange.getRates().get("USD")) : ""));
        }
    }

    /**
     * Get and baseUri.
     * @param uri uri
     * @return URI
     */
    private URI getBaseUri(String uri) {
        URI baseUri;
        try {
            baseUri = new URI(uri);
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
            baseUri = null;
        }
        return baseUri;
    }
}
