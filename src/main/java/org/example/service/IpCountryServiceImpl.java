package org.example.service;

import org.example.dto.IpDirectionResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class IpCountryServiceImpl{
    private static final Logger LOGGER = LoggerFactory.getLogger(IpCountryServiceImpl.class.getCanonicalName());

    /**
     * do an GET for IpCountryInfo
     * @param baseUri
     * @return
     */
    public IpDirectionResponseDto ipCountry(URI baseUri) {
        LOGGER.debug("Init method");
        IpDirectionResponseDto response = null;
        try {
            RestTemplate rest = new RestTemplate();
            response = rest.getForObject(baseUri,IpDirectionResponseDto.class);

        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
        }
        return response;
    }
}
