package org.example.service;

import org.example.dto.ExchangeResponseDto;
import org.example.service.feign.ExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExchangeServiceImpl implements ExchangeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeServiceImpl.class.getCanonicalName());

    @Autowired
    private ExchangeService exchangeService;

    /**
     * Do an exchange GET Operation.
     * @param base base
     * @return ExchangeResponseDto
     */
    @Override
    public ExchangeResponseDto exchange(String base) {
        LOGGER.debug("Init method");
        ExchangeResponseDto response = null;
        try {
            response = exchangeService.exchange(base);
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
        }
        return response;
    }
}
