package org.example.dao;

import org.example.config.RedisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


@Component
public class RedisConnectionFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisConnectionFactory.class.getCanonicalName());

    @Autowired
    private RedisConfig config;

    private JedisPool jedisPool;

    /**
     * Init a Jedis pool.
     */
    public void initJedisPool() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setBlockWhenExhausted(config.getBlockWhenExhausted());
        jedisPoolConfig.setMaxWaitMillis(config.getMaxWaitMillis());
        jedisPoolConfig.setFairness(config.getFairness());
        jedisPoolConfig.setMinEvictableIdleTimeMillis(config.getMinEvictableIdleMillis());
        jedisPoolConfig.setMaxTotal(config.getMaxConnections());
        jedisPoolConfig.setMaxIdle(config.getMaxIdleConnections());
        jedisPool = new JedisPool(jedisPoolConfig, config.getIp(), config.getPort(), config.getDefaultTimeout());
    }

    /**
     * Get connection from Jedis pool.
     * @return Jedis
     */
    public Jedis getConnectionFromPool() {
        Jedis redis = null;
        try {
            if (jedisPool == null) {
               initJedisPool();
            }
            redis = jedisPool.getResource();
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
        }
        return redis;
    }

    /**
     * Close connections and threads from Jedis
     * @param redis redis
     */
    public void closeConnection(Jedis redis) {
        try {
            if (redis != null) {
                redis.close();
                LOGGER.error("Closed");
            }
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
        }
    }
}
