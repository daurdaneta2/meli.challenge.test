package org.example.dao;

import org.apache.commons.lang3.StringUtils;
import org.example.dto.CountryDataDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RedisThreadDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisThreadDao.class.getCanonicalName());

    @Autowired
    private RedisConnectionFactory factory;
    private Jedis redis;

    /**
     * Set Distance for future reports.
     * @param country country
     * @param distance distance
     */
    public void incCountriesData(String country, String distance) {
        try {
            getConnection();
            redis.incr(String.format("MELI_DISTANCE_%s_%s", country, distance));
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
            closeConnection();
        } finally {
            closeConnection();
        }
    }

    /**
     * Get countries data.
     * @param countries countries
     * @return List of info
     */
    public List<CountryDataDto> getCountriesData(Set<String> countries) {
        List<CountryDataDto> response = new ArrayList<>();
        try {
            getConnection();
            for (String item : countries) {
                LOGGER.debug("Item : {}", item);
                String registry = redis.get(item);
                String dataCountry = item.replace("MELI_DISTANCE_", "");
                String country = dataCountry.split("_")[0];
                Integer distance = Integer.valueOf(dataCountry.split("_")[1]);
                LOGGER.debug("country {} - distance {}", country, distance);
                Integer iteration = Integer.valueOf(registry);
                response.add(new CountryDataDto(country, distance, iteration));
            }
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
            response = null;
            closeConnection();
        } finally {
            closeConnection();
        }
        return response;
    }

    /**
     * Get all distance registry.
     * @return Set
     */
    public Set<String> getDistanceRegistry() {
        Set<String> response;
        try {
            getConnection();
            response = redis.keys("MELI_DISTANCE_*");
            LOGGER.debug("Set of keys : {}", response);
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
            response = null;
            closeConnection();
        } finally {
            closeConnection();
        }
        return response;
    }

    /**
     * Get Connection from redis pool.
     * @return Jedis
     */
    private Jedis getConnection() {
        LOGGER.trace("Inside method...");
        try {
            if (redis == null) {
                redis = factory.getConnectionFromPool();
            }
        } catch (Exception e) {
            LOGGER.error("Exception connecting REDIS", e);
        }
        LOGGER.trace("Finishing method...");
        return redis;
    }

    /**
     * Close redis connection.
     */
    public void closeConnection() {
        try {
            factory.closeConnection(redis);
        } catch (Exception ex) {
            LOGGER.error("Exception", ex);
        }
    }
}
