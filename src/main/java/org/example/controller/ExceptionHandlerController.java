package org.example.controller;

import org.example.dto.ErrorHandlerExceptionDto;
import org.example.exceptions.BadParamRequestException;
import org.example.exceptions.IpNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class.getCanonicalName());
    @ExceptionHandler(BadParamRequestException.class)
    public final ResponseEntity<ErrorHandlerExceptionDto> handleBadRequestExceptions(Exception ex, WebRequest request,
                                                                           HttpServletRequest httpRequest) {
        return  new ResponseEntity<>(new ErrorHandlerExceptionDto("BAD_REQUEST"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IpNotFoundException.class)
    public final ResponseEntity<ErrorHandlerExceptionDto> handleIpNotFound(Exception ex, WebRequest request,
                                                                                     HttpServletRequest httpRequest) {
        return  new ResponseEntity<>(new ErrorHandlerExceptionDto("NOT_FOUND"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorHandlerExceptionDto> handleExceptions(Exception ex, WebRequest request,
                                                         HttpServletRequest httpRequest) {
        LOGGER.error("Exception", ex);
        return  new ResponseEntity<>(new ErrorHandlerExceptionDto("INTERNAL_SERVER_ERROR"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
