package org.example.controller;

import org.apache.commons.lang3.StringUtils;
import org.example.dto.IpInformationResponseDto;
import org.example.dto.ReportResponseDto;
import org.example.exceptions.BadParamRequestException;
import org.example.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/challenge")
public class AppController {

    @Autowired
    private AppService service;

    @GetMapping("/evaluate")
    public ResponseEntity<IpInformationResponseDto> evaluate(@RequestParam(name = "ip") String ip) {
        validateValues(ip);
        return service.processIp(ip);
    }

    @GetMapping("/report")
    public ResponseEntity<ReportResponseDto> report() {
        return service.generateReport();
    }

    private void validateValues(String ... values) throws BadParamRequestException {
        for (String item : values) {
            if (StringUtils.isBlank(item)) {
                throw new BadParamRequestException();
            }
        }
    }
}
