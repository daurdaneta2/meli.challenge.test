package org.example.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("redis.properties")
public class RedisConfig {
    private String ip;
    private Integer port;
    private Integer defaultTimeout = 2000;
    private Boolean blockWhenExhausted = true;
    private Boolean fairness = true;
    private Integer maxConnections = 300;
    private Integer maxIdleConnections = 30;
    private Integer maxWaitMillis = -1;
    private Integer minEvictableIdleMillis = 60000;

    /**
     * Get ip value.
     *
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set ip value.
     *
     * @param ip ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Get port value.
     *
     * @return port
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Set port value.
     *
     * @param port port
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * Get defaultTimeout value.
     *
     * @return defaultTimeout
     */
    public Integer getDefaultTimeout() {
        return defaultTimeout;
    }

    /**
     * Set defaultTimeout value.
     *
     * @param defaultTimeout defaultTimeout
     */
    public void setDefaultTimeout(Integer defaultTimeout) {
        this.defaultTimeout = defaultTimeout;
    }

    /**
     * Get blockWhenExhausted value.
     *
     * @return blockWhenExhausted
     */
    public Boolean getBlockWhenExhausted() {
        return blockWhenExhausted;
    }

    /**
     * Set blockWhenExhausted value.
     *
     * @param blockWhenExhausted blockWhenExhausted
     */
    public void setBlockWhenExhausted(Boolean blockWhenExhausted) {
        this.blockWhenExhausted = blockWhenExhausted;
    }

    /**
     * Get fairness value.
     *
     * @return fairness
     */
    public Boolean getFairness() {
        return fairness;
    }

    /**
     * Set fairness value.
     *
     * @param fairness fairness
     */
    public void setFairness(Boolean fairness) {
        this.fairness = fairness;
    }

    /**
     * Get maxConnections value.
     *
     * @return maxConnections
     */
    public Integer getMaxConnections() {
        return maxConnections;
    }

    /**
     * Set maxConnections value.
     *
     * @param maxConnections maxConnections
     */
    public void setMaxConnections(Integer maxConnections) {
        this.maxConnections = maxConnections;
    }

    /**
     * Get maxIdleConnections value.
     *
     * @return maxIdleConnections
     */
    public Integer getMaxIdleConnections() {
        return maxIdleConnections;
    }

    /**
     * Set maxIdleConnections value.
     *
     * @param maxIdleConnections maxIdleConnections
     */
    public void setMaxIdleConnections(Integer maxIdleConnections) {
        this.maxIdleConnections = maxIdleConnections;
    }

    /**
     * Get maxWaitMillis value.
     *
     * @return maxWaitMillis
     */
    public Integer getMaxWaitMillis() {
        return maxWaitMillis;
    }

    /**
     * Set maxWaitMillis value.
     *
     * @param maxWaitMillis maxWaitMillis
     */
    public void setMaxWaitMillis(Integer maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    /**
     * Get minEvictableIdleMillis value.
     *
     * @return minEvictableIdleMillis
     */
    public Integer getMinEvictableIdleMillis() {
        return minEvictableIdleMillis;
    }

    /**
     * Set minEvictableIdleMillis value.
     *
     * @param minEvictableIdleMillis minEvictableIdleMillis
     */
    public void setMinEvictableIdleMillis(Integer minEvictableIdleMillis) {
        this.minEvictableIdleMillis = minEvictableIdleMillis;
    }
}
