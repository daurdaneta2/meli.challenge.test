package org.example.dto;

public class CountryLanguagesDto {
    private String name;
    private String iso639_1;
    private String nativeName;

    /**
     * Get iso639_1 value.
     *
     * @return iso639_1
     */
    public String getIso639_1() {
        return iso639_1;
    }

    /**
     * Set iso639_1 value.
     *
     * @param iso639_1 iso639_1
     */
    public void setIso639_1(String iso639_1) {
        this.iso639_1 = iso639_1;
    }

    /**
     * Get nativeName value.
     *
     * @return nativeName
     */
    public String getNativeName() {
        return nativeName;
    }

    /**
     * Set nativeName value.
     *
     * @param nativeName nativeName
     */
    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    /**
     * Get name value.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name value.
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
}
