package org.example.dto;

import java.util.Map;

public class CountryInfoDto {
    private String alpha3Code;
    private Double[] latlng;
    private String[] timezones;
    private CountryCurrenciesDto[] currencies;
    private CountryLanguagesDto[] languages;
    private Map<String, String> translations;

    /**
     * Get alpha3Code value.
     *
     * @return alpha3Code
     */
    public String getAlpha3Code() {
        return alpha3Code;
    }

    /**
     * Set alpha3Code value.
     *
     * @param alpha3Code alpha3Code
     */
    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    /**
     * Get translations value.
     *
     * @return translations
     */
    public Map<String, String> getTranslations() {
        return translations;
    }

    /**
     * Set translations value.
     *
     * @param translations translations
     */
    public void setTranslations(Map<String, String> translations) {
        this.translations = translations;
    }

    /**
     * Get latlng value.
     *
     * @return latlng
     */
    public Double[] getLatlng() {
        return latlng;
    }

    /**
     * Set latlng value.
     *
     * @param latlng latlng
     */
    public void setLatlng(Double[] latlng) {
        this.latlng = latlng;
    }

    /**
     * Get timezones value.
     *
     * @return timezones
     */
    public String[] getTimezones() {
        return timezones;
    }

    /**
     * Set timezones value.
     *
     * @param timezones timezones
     */
    public void setTimezones(String[] timezones) {
        this.timezones = timezones;
    }

    /**
     * Get currencies value.
     *
     * @return currencies
     */
    public CountryCurrenciesDto[] getCurrencies() {
        return currencies;
    }

    /**
     * Set currencies value.
     *
     * @param currencies currencies
     */
    public void setCurrencies(CountryCurrenciesDto[] currencies) {
        this.currencies = currencies;
    }

    /**
     * Get languages value.
     *
     * @return languages
     */
    public CountryLanguagesDto[] getLanguages() {
        return languages;
    }

    /**
     * Set languages value.
     *
     * @param languages languages
     */
    public void setLanguages(CountryLanguagesDto[] languages) {
        this.languages = languages;
    }
}
