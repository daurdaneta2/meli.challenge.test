package org.example.dto;

public class CountryDataDto {
    private String country;
    private Integer distance;
    private Integer invoice;

    /**
     * Constructor.
     * @param country country
     * @param distance distance
     * @param invoice invoice
     */
    public CountryDataDto(String country, Integer distance, Integer invoice) {
        this.country = country;
        this.distance = distance;
        this.invoice = invoice;
    }

    /**
     * Get country value.
     *
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Set country value.
     *
     * @param country country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Get distance value.
     *
     * @return distance
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     * Set distance value.
     *
     * @param distance distance
     */
    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    /**
     * Get invoice value.
     *
     * @return invoice
     */
    public Integer getInvoice() {
        return invoice;
    }

    /**
     * Set invoice value.
     *
     * @param invoice invoice
     */
    public void setInvoice(Integer invoice) {
        this.invoice = invoice;
    }
}
