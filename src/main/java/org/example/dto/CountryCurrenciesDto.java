package org.example.dto;

public class CountryCurrenciesDto {
    private String code;
    private String name;

    /**
     * Get code value.
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * Set code value.
     *
     * @param code code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get name value.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name value.
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
}
