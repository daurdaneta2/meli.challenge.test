package org.example.dto;

import com.google.gson.annotations.SerializedName;

public class ErrorHandlerExceptionDto {
    @SerializedName("mensaje")
    private String message;

    /**
     * Constructor.
     * @param message message
     */
    public ErrorHandlerExceptionDto(String message) {
        this.message = message;
    }

    /**
     * Get message value.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set message value.
     *
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
