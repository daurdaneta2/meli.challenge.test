package org.example.dto;

import com.google.gson.annotations.SerializedName;

public class IpInformationResponseDto {
    private String ip;
    @SerializedName("nombre")
    private String name;
    @SerializedName("fecha_actual")
    private String date;
    @SerializedName("codigo_iso")
    private String isoName;
    @SerializedName("idiomas")
    private String[] languages;
    @SerializedName("zona_horaria")
    private String[] hourZone;
    @SerializedName("Distancia")
    private String distance;
    @SerializedName("moneda_local")
    private String currency;

    /**
     * Get date value.
     *
     * @return date
     */
    public String getDate() {
        return date;
    }

    /**
     * Set date value.
     *
     * @param date date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Get ip value.
     *
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set ip value.
     *
     * @param ip ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Get name value.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name value.
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get isoName value.
     *
     * @return isoName
     */
    public String getIsoName() {
        return isoName;
    }

    /**
     * Set isoName value.
     *
     * @param isoName isoName
     */
    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    /**
     * Get languages value.
     *
     * @return languages
     */
    public String[] getLanguages() {
        return languages;
    }

    /**
     * Set languages value.
     *
     * @param languages languages
     */
    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    /**
     * Get hourZone value.
     *
     * @return hourZone
     */
    public String[] getHourZone() {
        return hourZone;
    }

    /**
     * Set hourZone value.
     *
     * @param hourZone hourZone
     */
    public void setHourZone(String[] hourZone) {
        this.hourZone = hourZone;
    }

    /**
     * Get distance value.
     *
     * @return distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * Set distance value.
     *
     * @param distance distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     * Get currency value.
     *
     * @return currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Set currency value.
     *
     * @param currency currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
