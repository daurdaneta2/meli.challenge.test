package org.example.dto;

import java.util.Map;

public class ExchangeResponseDto {
    private Map<String, Float> rates;
    private String base;
    private String date;

    /**
     * Get rates value.
     *
     * @return rates
     */
    public Map<String, Float> getRates() {
        return rates;
    }

    /**
     * Set rates value.
     *
     * @param rates rates
     */
    public void setRates(Map<String, Float> rates) {
        this.rates = rates;
    }

    /**
     * Get base value.
     *
     * @return base
     */
    public String getBase() {
        return base;
    }

    /**
     * Set base value.
     *
     * @param base base
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * Get date value.
     *
     * @return date
     */
    public String getDate() {
        return date;
    }

    /**
     * Set date value.
     *
     * @param date date
     */
    public void setDate(String date) {
        this.date = date;
    }
}
