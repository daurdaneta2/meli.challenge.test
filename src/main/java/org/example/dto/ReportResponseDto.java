package org.example.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;

public class ReportResponseDto {
    @SerializedName("mensaje")
    private String message;
    @SerializedName("distancia_mayor")
    private String longDistance = null;
    @SerializedName("pais_distancia_mayor")
    private String longDistanceCountry;
    @SerializedName("pais_distancia_menor")
    private String shortDistanceCountry;
    @SerializedName("distancia_menor")
    private String shortDistance = null;
    @SerializedName("distancia_promedio")
    private String averageDistance;

    /**
     * Get longDistanceCountry value.
     *
     * @return longDistanceCountry
     */
    public String getLongDistanceCountry() {
        return longDistanceCountry;
    }

    /**
     * Set longDistanceCountry value.
     *
     * @param longDistanceCountry longDistanceCountry
     */
    public void setLongDistanceCountry(String longDistanceCountry) {
        this.longDistanceCountry = longDistanceCountry;
    }

    /**
     * Get shortDistanceCountry value.
     *
     * @return shortDistanceCountry
     */
    public String getShortDistanceCountry() {
        return shortDistanceCountry;
    }

    /**
     * Set shortDistanceCountry value.
     *
     * @param shortDistanceCountry shortDistanceCountry
     */
    public void setShortDistanceCountry(String shortDistanceCountry) {
        this.shortDistanceCountry = shortDistanceCountry;
    }

    /**
     * Empty constructor.
     */
    public ReportResponseDto() {
    }

    /**
     * Constructor.
     * @param message message
     */
    public ReportResponseDto(String message) {
        this.message = message;
    }

    /**
     * Get message value.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set message value.
     *
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get longDistance value.
     *
     * @return longDistance
     */
    public String getLongDistance() {
        return longDistance;
    }

    /**
     * Set longDistance value.
     *
     * @param longDistance longDistance
     */
    public void setLongDistance(String longDistance) {
        this.longDistance = longDistance;
    }

    /**
     * Get shortDistance value.
     *
     * @return shortDistance
     */
    public String getShortDistance() {
        return shortDistance;
    }

    /**
     * Set shortDistance value.
     *
     * @param shortDistance shortDistance
     */
    public void setShortDistance(String shortDistance) {
        this.shortDistance = shortDistance;
    }

    /**
     * Get averageDistance value.
     *
     * @return averageDistance
     */
    public String getAverageDistance() {
        return averageDistance;
    }

    /**
     * Set averageDistance value.
     *
     * @param averageDistance averageDistance
     */
    public void setAverageDistance(String averageDistance) {
        this.averageDistance = averageDistance;
    }
}
