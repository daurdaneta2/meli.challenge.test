package org.example.dto;

public class IpDirectionResponseDto {
    private String countryCode;
    private String countryCode3;
    private String countryName;

    /**
     * Get countryCode value.
     *
     * @return countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Set countryCode value.
     *
     * @param countryCode countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Get countryCode3 value.
     *
     * @return countryCode3
     */
    public String getCountryCode3() {
        return countryCode3;
    }

    /**
     * Set countryCode3 value.
     *
     * @param countryCode3 countryCode3
     */
    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }

    /**
     * Get countryName value.
     *
     * @return countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Set countryName value.
     *
     * @param countryName countryName
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
