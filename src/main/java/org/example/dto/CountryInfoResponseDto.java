package org.example.dto;

import java.util.Map;

public class CountryInfoResponseDto {
    private CountryInfoDto[] countries;

    /**
     * Get countries value.
     *
     * @return countries
     */
    public CountryInfoDto[] getCountries() {
        return countries;
    }

    /**
     * Set countries value.
     *
     * @param countries countries
     */
    public void setCountries(CountryInfoDto[] countries) {
        this.countries = countries;
    }
}
