package org.example.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Component
public class Utils {
    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class.getCanonicalName());

    /**
     * Calculate Distance between Latitude and Longitude.
     * @param origin Origin coordinates
     * @param destination Destination coordinates
     * @return Distance
     */
    public Double takeDistanceLatLng(Double[] origin, Double[] destination) {
        LOGGER.debug("origin {} - destination {}", origin, destination);
        //Where index 0 is Latitude
        //Where index 1 is Longitude
        double deltaLat = Math.toRadians(origin[0] - destination[0]);
        double deltaLng = Math.toRadians(origin[1] - destination[1]);

        LOGGER.debug("delta lat: {}", deltaLat);
        LOGGER.debug("delta lng: {}", deltaLng);

        //"a = [sin ² (Δlat / 2) + cos (lat1)] x cos (lat2) x sin ² (Δlong / 2)"
        double a = Math.pow(Math.sin(deltaLat / 2),2)
                + Math.cos(Math.toRadians(origin[0])) * Math.cos(Math.toRadians(destination[0]))
                * Math.pow(Math.sin(deltaLng / 2), 2);

        LOGGER.debug("a: {}", a);

        //"c = 2 x arctan (√ a / √ (1-a))"
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        LOGGER.debug("C: {}", c);

        double response = 6371 * c;

        LOGGER.debug("response: {}", response);

        return response;
    }

    /**
     * Parse Date with UTC Timezone offset.
     * @param timezone timezone
     * @return Parse Date.
     */
    public String getTimeToTimezone(String timezone) {
        ZoneOffset zoneOffSet = ZoneOffset.of(timezone);
        OffsetTime time = OffsetTime.now(zoneOffSet);

        return time.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }
}
