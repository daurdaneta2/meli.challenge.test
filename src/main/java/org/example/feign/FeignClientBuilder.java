package org.example.feign;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.Request;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.httpclient.ApacheHttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

public class FeignClientBuilder {
    private Client httpClient;
    private Integer connectionTimeout;
    private Integer readTimeout;

    private Decoder decoder;
    private Encoder encoder;
    private Contract contract;

    public static FeignClientBuilder initializeFeignClient() {
        return new FeignClientBuilder();
    }

    private FeignClientBuilder() {
    }

    public FeignClientBuilder withCustomHttpClient(final Client httpClient) {
        this.httpClient = httpClient;
        return this;
    }

    public FeignClientBuilder withCustomDecoder(final Decoder decoder) {
        this.decoder = decoder;
        return this;
    }

    public FeignClientBuilder withCustomEncoder(final Encoder encoder) {
        this.encoder = encoder;
        return this;
    }

    public FeignClientBuilder withCustomContract(final Contract contract) {
        this.contract = contract;
        return this;
    }

    public FeignClientBuilder withDefautlHttpClient(final Integer maxHttpConnectionsInPool) {

        final CloseableHttpClient closeableHttpClient = HttpClientBuilder.create()
                .setMaxConnTotal(maxHttpConnectionsInPool)
                .build();
        this.httpClient = new ApacheHttpClient(closeableHttpClient);
        return this;
    }

    public FeignClientBuilder withDefaultDecoder() {
        this.decoder = getDefaultDecoder();
        return this;
    }

    public FeignClientBuilder withDefaultEncoder() {
        this.encoder = getDefaultEncoder();
        return this;
    }

    public FeignClientBuilder withDefaultContract() {
        this.contract = getFeignClientContract();
        return this;
    }

    public FeignClientBuilder withConnectionTimeout(final Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    public FeignClientBuilder withReadTimeout(final Integer readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public <T> T buildForSpecification(
            Class<T> feignClientContractClass,
            final String url) {
        final Request.Options feignClientOptions = new Request.Options(connectionTimeout, readTimeout);

        return Feign.builder()
                .contract(contract)
                .encoder(encoder)
                .decoder(decoder)
                .client(httpClient)
                .options(feignClientOptions)
                .target(feignClientContractClass, url);
    }

    private Decoder getDefaultDecoder() {
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(
                new GsonHttpMessageConverter());
        return new ResponseEntityDecoder(
                new SpringDecoder(objectFactory));
    }

    private Encoder getDefaultEncoder() {
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(
                new GsonHttpMessageConverter());
        return new SpringEncoder(objectFactory);
    }

    private SpringMvcContract getFeignClientContract() {
        return new SpringMvcContract();
    }
}
