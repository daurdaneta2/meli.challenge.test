package org.example.feign;

import org.example.service.feign.CountryInfoService;
import org.example.service.feign.ExchangeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class FeignClientsConfiguration {

    /**
     * Generate FeignClient of CountryInfoService.
     * @param maxHttpPoolConnection maxHttpPoolConnection
     * @param connectionTimeout connectionTimeout
     * @param readTimeout readTimeout
     * @param url url
     * @return FeignClient
     */
    @Bean(name = "countryInfoService")
    public CountryInfoService configureCountryInfoFeignClient(
            @Value("${feign.country.info.max.http.pool}") final Integer maxHttpPoolConnection,
            @Value("${feign.country.info.connection.timeout}") final Integer connectionTimeout,
            @Value("${feign.country.info.read.timeout}") final Integer readTimeout,
            @Value("${feign.country.info.url}") final String url
    ) {
        return FeignClientBuilder.initializeFeignClient()
                .withDefautlHttpClient(maxHttpPoolConnection)
                .withDefaultContract()
                .withDefaultDecoder()
                .withDefaultEncoder()
                .withConnectionTimeout(connectionTimeout)
                .withReadTimeout(readTimeout)
                .buildForSpecification(CountryInfoService.class, url);
    }


    /**
     * Generate FeignClient of exchangeService
     * @param maxHttpPoolConnection maxHttpPoolConnection
     * @param connectionTimeout connectionTimeout
     * @param readTimeout readTimeout
     * @param url url
     * @return ExchangeService
     */
    @Bean(name = "exchangeService")
    public ExchangeService configureExchangeFeignClient(
            @Value("${feign.exchange.max.http.pool}") final Integer maxHttpPoolConnection,
            @Value("${feign.exchange.connection.timeout}") final Integer connectionTimeout,
            @Value("${feign.exchange.read.timeout}") final Integer readTimeout,
            @Value("${feign.exchange.url}") final String url
    ) {
        return FeignClientBuilder.initializeFeignClient()
                .withDefautlHttpClient(maxHttpPoolConnection)
                .withDefaultContract()
                .withDefaultDecoder()
                .withDefaultEncoder()
                .withConnectionTimeout(connectionTimeout)
                .withReadTimeout(readTimeout)
                .buildForSpecification(ExchangeService.class, url);
    }
}
