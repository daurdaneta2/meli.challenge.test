package org.example;

import org.assertj.core.util.Arrays;
import org.example.controller.AppController;
import org.example.dao.RedisThreadDao;
import org.example.dto.CountryCurrenciesDto;
import org.example.dto.CountryDataDto;
import org.example.dto.CountryInfoDto;
import org.example.dto.CountryInfoResponseDto;
import org.example.dto.CountryLanguagesDto;
import org.example.dto.ExchangeResponseDto;
import org.example.dto.IpDirectionResponseDto;
import org.example.dto.IpInformationResponseDto;
import org.example.dto.ReportResponseDto;
import org.example.service.CountryInfoServiceImpl;
import org.example.service.ExchangeServiceImpl;
import org.example.service.IpCountryServiceImpl;
import org.example.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeneralTest {

    @Autowired
    private Utils utils;
    @Autowired
    private AppController controller;
    @MockBean
    private RedisThreadDao redisThreadDao;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @MockBean
    private IpCountryServiceImpl ipCountryService;
    @MockBean
    private CountryInfoServiceImpl countryInfoService;
    @MockBean
    private ExchangeServiceImpl exchangeService;


    private final Double[] argLatLng = {-34d, -64d};
    private final Double[] destLatLng = {40d, -4d};
    private final Integer finalDistance = 10274;

    private final String ip = "186.31.183.139";

    @Before
    public void setUp() throws Exception {
        mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void testGetDistanceUtils() {
        Double distance = utils.takeDistanceLatLng(argLatLng, destLatLng);
        assertTrue(distance != null && finalDistance.equals(distance.intValue()));
    }

    @Test
    public void testMethodIp() {
        IpDirectionResponseDto responseIp = new IpDirectionResponseDto();
        responseIp.setCountryCode("ES");
        responseIp.setCountryCode3("ESP");
        responseIp.setCountryName("Spain");

        when(ipCountryService.ipCountry(Mockito.any())).thenReturn(responseIp);

        CountryInfoResponseDto responseCountry = new CountryInfoResponseDto();

        CountryInfoDto[] countries = {new CountryInfoDto()};
        countries[0].setLatlng(Arrays.array(40d, -4d));
        countries[0].setTimezones(Arrays.array("UTC","UTC+01:00"));
        countries[0].setCurrencies(Arrays.array(new CountryCurrenciesDto()));
        countries[0].getCurrencies()[0].setCode("EUR");
        countries[0].getCurrencies()[0].setName("Euro");
        countries[0].setLanguages(Arrays.array(new CountryLanguagesDto()));
        countries[0].getLanguages()[0].setName("Spanish");
        countries[0].getLanguages()[0].setIso639_1("es");
        countries[0].getLanguages()[0].setNativeName("Español");
        countries[0].setTranslations(new HashMap<>());
        countries[0].getTranslations().put("es", "España");

        //responseCountry.setCountries(countries);
        when(countryInfoService.countryInfo(Mockito.anyString())).thenReturn(countries);

        ExchangeResponseDto exchange = new ExchangeResponseDto();
        exchange.setBase("EUR");
        exchange.setRates(new HashMap<>());
        exchange.getRates().put("USD", 1.174f);
        when(exchangeService.exchange(Mockito.anyString())).thenReturn(exchange);

        doNothing().when(redisThreadDao).incCountriesData(Mockito.anyString(), Mockito.anyString());

        ResponseEntity<IpInformationResponseDto> response = controller.evaluate(ip);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testMethodIpNotFoundMethod() throws Exception {
        mockMvc.perform(get("/challenge/eval", "")
                .contentType(MediaType.ALL_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    public void testMethodReport() {
        Set<String> countries = new HashSet<>();
        countries.add("MELI_DISTANCE_BRASIL_2862");
        countries.add("MELI_DISTANCE_ESPAÑA_10040");

        List<CountryDataDto> dataCountry = new ArrayList<>();
        dataCountry.add(new CountryDataDto("BRASIL", 2862, 10));
        dataCountry.add(new CountryDataDto("ESPAÑA", 10040, 5));

        when(redisThreadDao.getDistanceRegistry()).thenReturn(countries);
        when(redisThreadDao.getCountriesData(Mockito.any())).thenReturn(dataCountry);

        ResponseEntity<ReportResponseDto> response = controller.report();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testMethodReportNoDataKeys() {
        Set<String> countries = new HashSet<>();
        when(redisThreadDao.getDistanceRegistry()).thenReturn(countries);
        ResponseEntity<ReportResponseDto> response = controller.report();
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testMethodReportFailGettingIterations() {
        Set<String> countries = new HashSet<>();
        countries.add("MELI_DISTANCE_BRASIL_2862");
        countries.add("MELI_DISTANCE_ESPAÑA_10040");

        List<CountryDataDto> dataCountry = null;

        when(redisThreadDao.getDistanceRegistry()).thenReturn(countries);
        when(redisThreadDao.getCountriesData(Mockito.any())).thenReturn(dataCountry);

        ResponseEntity<ReportResponseDto> response = controller.report();
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testMethodReportNotFoundIterations() {
        Set<String> countries = new HashSet<>();
        countries.add("MELI_DISTANCE_BRASIL_2862");
        countries.add("MELI_DISTANCE_ESPAÑA_10040");

        List<CountryDataDto> dataCountry = new ArrayList<>();

        when(redisThreadDao.getDistanceRegistry()).thenReturn(countries);
        when(redisThreadDao.getCountriesData(Mockito.any())).thenReturn(dataCountry);

        ResponseEntity<ReportResponseDto> response = controller.report();
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }
}
